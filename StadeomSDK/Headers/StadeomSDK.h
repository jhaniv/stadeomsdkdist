//
//  StadeomSDK.h
//  StadeomSDK
//
//  Created by Yaniv Solnik on 1/9/15.
//  Copyright (c) 2015 Stadeom Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface StadeomSDK : NSObject

typedef void (^CompletionHandlerType)(id result);

typedef NS_ENUM(NSUInteger, STLoginRequestReason) {
    
    STLAddComment,
    STLLike,
    STLFlag
};

typedef void (^LoginRequestHandlerType)(id result, STLoginRequestReason loginRequestReason);
typedef void (^ShareItemHandlerType)(NSString *itemUrlStr);

+(void)initWithApiKey:(NSString*)apiKey andLaunchOptions:(NSDictionary*)launchOptions;
+(UIView*)addEventWallToView:(UIView*)rootView withEventId:(NSString*)eventId withOptions:(NSDictionary*)options;
+(void)setDeviceToken:(NSString*)deviceToken;
+(void)receivedRemoteNotification:(NSDictionary *)userInfo;

+(void)setRequireLoginBlock:(LoginRequestHandlerType)handler;
+(void)setShareItemBlock:(ShareItemHandlerType)handler;

//+(void)createEvent;
//+(void)startRecordingWithCompletionHandler:(CompletionHandlerType)handler;
//+(void)getEvents:(NSDictionary*)options andCompletionHandler:(CompletionHandlerType)handler;
//+(void)registerForNotifications;




@end
