Pod::Spec.new do |s|
  s.name         = "StadeomSDK"
  s.version      = "0.1.10"
  s.summary      = "iOS library for Stadeom"
  s.homepage     = "http://stadeom.com.com"
  s.author       = { "Stadeom, Inc" => "dev@stadeom.com" }
  s.platform     = :ios 
  s.source       = { :git => "https://bitbucket.org/jhaniv/stadeomsdkdist.git", :tag => "1.0.0" }
  s.source_files =  'StadeomSDK/Headers/*.h'
  s.preserve_path = 'StadeomSDK/libStadeomSDK.a'
  s.preserve_path = 'StadeomSDK/StadeomSDKResources.bundle'
  s.vendored_libraries = 'StadeomSDK/libStadeomSDK.a'
  s.ios.deployment_target = '7.0'
  s.frameworks = "MobileCoreServices", "CoreMedia", "AudioToolbox", "SystemConfiguration", 
  "CoreAudio", "MediaPlayer", "AVFoundation"
  
  s.resource = 'StadeomSDK/StadeomSDKResources.bundle'

  #s.dependency "Lambda-Alert", "~>1.0.1"
  #s.dependency "SVProgressHUD"
  #s.dependency "AFNetworking", "~>2.3.1"
  #s.dependency "SSKeychain"
  #s.dependency "UIColor-HexString", "~> 1.0.0"
  #s.dependency "SDWebImage", "~>3.6"
  #s.dependency "Masonry"
  #s.dependency "XLForm", "~> 2.1.0"
  #s.dependency "SVPullToRefresh"

  s.requires_arc = true
  s.xcconfig  =  { 'LIBRARY_SEARCH_PATHS' => '"$(PODS_ROOT)/StadeomSDK"',
                   'HEADER_SEARCH_PATHS' => '"${PODS_ROOT}/Headers/StadeomSDK"' }
  s.license      = {
    :type => 'Copyright',
    :text => <<-LICENSE
      Copyright 2015 Stadeom, Inc. All rights reserved.
      LICENSE
  }
end